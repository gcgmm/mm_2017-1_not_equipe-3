function Peca(tamX, tamY, pxI, pyI, pxC, pyC) {
  this.tx = tamX;
  this.ty = tamY;
  this.pxImg = pxI;
  this.pyImg = pyI;
  this.pxCnv = pxC;
  this.pyCnv = pyC;
  this.vizinhos = [this];
};

Peca.prototype =  {
  desenhar: function(ctx , img) {
    ctx.strokeRect(this.pxCnv, this.pyCnv, this.tx, this.ty); 
    ctx.drawImage(img, this.pxImg, this.pyImg, this.tx, this.ty, this.pxCnv, this.pyCnv, this.tx, this.ty);
  },
  target: function(xs, ys) {
    for (i in this.vizinhos) {
      var obj =  this.vizinhos[i];
      if(obj.pxCnv <= xs && (obj.pxCnv + obj.tx) >= xs && obj.pyCnv <= ys && (obj.pyCnv + obj.ty) >= ys){
          return true;
      }
    }
  },
  desenharObj: function(ctx , img, dx, dy) {
    ctx.strokeRect(this.pxCnv+dx, this.pyCnv+dy, this.tx, this.ty); 
    ctx.drawImage(img, this.pxImg, this.pyImg, this.tx, this.ty, this.pxCnv+dx, this.pyCnv+dy, this.tx, this.ty);
  }
};















