var cnv;
var ctx;
var NUM_ROWS = 2;
var NUM_COLS = 2;
var incrementador = 1;
var pecas = [];
var excluir = [];
var objAtual = null;
var gameImg;
var fundo;
var musicaFundo;

// variaveis do video
var pausas = [63.3, 107, 158];
var indice = 0;
var pausa = 0;
var inicio = 0;
var cw;
var ch;
var v;



window.onload = function() {
  cnv = document.getElementById("id_canvas");
  ctx = cnv.getContext("2d");
  var hm = new Hammer(cnv);
  hm.get('pan').set({ direction: Hammer.DIRECTION_ALL, threshold: 0});

  carregarMusicaFundo();
  
  var fundoInicial = new Image();
  fundoInicial.src = 'img/fundoInicial.png';
  fundoInicial.onload = function () {
    ctx.drawImage(fundoInicial, 0, 0, cnv.width, cnv.height);
  };

  hm.on('panstart', function(ev) {
    xs = ev.center.x - cnv.offsetLeft + document.body.scrollLeft;
    ys = ev.center.y - cnv.offsetTop + document.body.scrollTop;
    for (var i = pecas.length-1; i >= 0; i--){
      if(pecas[i].target(xs, ys)){
        objAtual =  pecas[i];
        pecas.splice(i, 1);
        break;
      }
    }
  });
  hm.on("panleft panright panup pandown", function(ev){
    if(objAtual != null){
      dx = ev.deltaX;
      dy = ev.deltaY;
      desenhar();
    }
  });
  hm.on("panend", function(ev){
    if(objAtual != null){
      for (a in objAtual.vizinhos){
        objAtual.vizinhos[a].pxCnv += dx;
        objAtual.vizinhos[a].pyCnv += dy;

        for (b in pecas){
          for (c in pecas[b].vizinhos){
            if(testeColisao(objAtual.vizinhos[a], pecas[b].vizinhos[c])){
              objAtual.vizinhos = objAtual.vizinhos.concat(pecas[b].vizinhos);
              pecas[b].vizinhos = [];
              excluir.push(b);
              break;
            }
          }
        }
      }
      dx = dy = 0;
      for (e in excluir) {
        pecas.splice(excluir[e], 1);
      }
      excluir = [];
      pecas.push(objAtual);
      objAtual = null;
      if(pecas.length == 1) {
        limparCanvas();
        pecas = [];
        fimDeJogo();
      }
    }
  });
};


function iniciarVideo() {
  v = document.getElementById('id_video');
  cw = cnv.clientWidth;
  ch = cnv.clientHeight;
  cnv.width = cw;
  cnv.height = ch;
  v.play();

  v.addEventListener('play', function(){
	draw(this, ctx, cw, ch);
  },false);
};

function draw(v, c, w, h){
  if(v.currentTime > pausas[indice]){
    v.pause();
    pausa = 1;
    indice = indice + 1;
    iniciarJogo();
  } else {
	  c.drawImage(v, 0, 0, w, h);
      setTimeout(draw, 20, v, c, w, h);
	  //botao();
    }
}

function ocultarExibir(el){
  var display = document.getElementById(el).style.display;
  if(display == "none")
  		document.getElementById(el).style.display = 'block';
  else
      document.getElementById(el).style.display = 'none';
};

function voltarAoVideo(){
  if(incrementador == 4){
    document.location.reload(true);
  }else{
    ocultarExibir('botaoProximo');
    pausa = 0;
    v.play();
  }
};


function iniciarJogo(){
  gameImg = new Image();
  gameImg.src = 'img/'+incrementador+'.png';
  gameImg.onload = function () {
    createPeca();
    desenhar();
    musicaFundo.play();
  };
};


function fimDeJogo(){
  musicaFundo.pause();
  musicaFundo.currentTime = 0.0;

  var marcador = new Image();
  marcador.src = 'img/marcador'+incrementador+'.png';
  marcador.onload = function (){
    ctx.drawImage(gameImg, 0, 0, cnv.width, cnv.height);
    ctx.drawImage(marcador, (cnv.width / 2) -100, (cnv.height / 2) -100, 200, 200);
  };
  incrementador++;

  NUM_ROWS = NUM_COLS = (NUM_ROWS + 1); // dificuldade do quebra-cabeça - com o menu isto sera alterado
  ocultarExibir('botaoProximo');
};


function menuHistoria(){
  var fundoescolha = new Image();
  fundoescolha.src = 'img/fundoHistoria.png';
  fundoescolha.onload = function (){
    ctx.drawImage(fundoescolha, 0, 0, cnv.width, cnv.height);
    ctx.fillRect((cnv.width / 2) -100, (cnv.height / 2) -100, 200, 200);
  };

  //inicia o video apos ser escolhido a historia
  //iniciarVideo();
};


function requestFullScreen(){
  var el = document.getElementById('jogo');
  // Supports most browsers and their versions.
  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen;
  if (requestMethod){
    // Native full screen.
    requestMethod.call(el);
  } else if (typeof window.ActiveXObject !== "undefined"){
    // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null){
      wscript.SendKeys("{F11}");
    }
  }
  
  ocultarExibir('startGame');
  cnv.height = 680; 
  cnv.width = 1300;
  limparCanvas();
 //menuHistoria();
  iniciarVideo();
};


function ocultarExibir(el){
  var display = document.getElementById(el).style.display;
  if(display == "none"){
      document.getElementById(el).style.display = 'block';
  } else {
      document.getElementById(el).style.display = 'none';
  }
};


function desenhar(){
  limparCanvas();
  ctx.drawImage(fundo, 0, 0, cnv.width, cnv.height);
  for (i in pecas){
    for (j in pecas[i].vizinhos){
      pecas[i].vizinhos[j].desenhar(ctx, gameImg);
    }
  }
  if(objAtual != null){
    for (k in objAtual.vizinhos){
      objAtual.vizinhos[k].desenharObj(ctx, gameImg, dx, dy);
    }
  }
};


function createPeca(){
  tamX = gameImg.width / NUM_COLS;
  tamY = gameImg.height / NUM_ROWS;
  for (var i = 0; i < NUM_ROWS; i++){
    for (var j = 0; j < NUM_COLS; j++){
      pecas.push(new Peca(tamX, tamY, tamX * j, tamY * i, randomPos(cnv.width, tamX), randomPos(cnv.height, tamY)));
    }
  }
};


function randomPos(dimCanvas, tamPeca){
  return Math.floor(Math.random() * (dimCanvas - tamPeca + 1));
};

function limparCanvas(){
  ctx.clearRect(0, 0, cnv.width, cnv.height);
};


function testeColisao(objA, objB){
  var tproporcao = (objAtual.tx * 1) / 100;

  //teste + 
  if((objA.pxCnv + objA.tx / 2) - tproporcao < (objB.pxCnv + (objB.tx / 2)) &&
     (objA.pxCnv + objA.tx / 2) + tproporcao > (objB.pxCnv + (objB.tx / 2)) ||
     (objA.pyCnv + objA.ty / 2) - tproporcao < (objB.pyCnv + (objB.ty / 2)) &&
     (objA.pyCnv + objA.ty / 2) + tproporcao > (objB.pyCnv + (objB.ty / 2))){
		 
     //encaixe para direita
     if(objA.pxCnv + objA.tx + tproporcao > objB.pxCnv &&
        objA.pxCnv + objA.tx - tproporcao < objB.pxCnv &&
        objA.pxImg + objA.tx == objB.pxImg && objA.pyImg == objB.pyImg){
          for (var i = 0; i < objB.vizinhos.length; i++){
            objB.vizinhos[i].pxCnv += (objA.pxCnv + objA.tx) - objB.pxCnv;
            //objB.vizinhos[i].pyCnv += (objA.pyCnv - objB.pyCnv);
          }
          return true; 
     }
	 //encaixe para esquerda
     if(objA.pxCnv - tproporcao < objB.pxCnv + objB.tx &&
        objA.pxCnv + tproporcao > objB.pxCnv + objB.tx &&
        objA.pxImg == objB.pxImg + objB.tx && objA.pyImg == objB.pyImg){
 
          return true; 
     }
	 //encaixe para baixo
     if(objA.pyCnv + objA.ty + tproporcao > objB.pyCnv &&
        objA.pyCnv + objA.ty - tproporcao < objB.pyCnv &&
        objA.pyImg + objA.ty == objB.pyImg && objA.pxImg == objB.pxImg){
		    
          return true; 
     } 
	 //encaixe para cima
     if(objA.pyCnv - tproporcao < objB.pyCnv + objB.ty &&
        objA.pyCnv + tproporcao > objB.pyCnv + objB.ty &&
        objA.pyImg == objB.pyImg + objB.ty && objA.pxImg == objB.pxImg){

          return true; 
     }   
  }   
  return false; 
};


function carregarMusicaFundo(){
  musicaFundo = new Audio();
  musicaFundo.src = 'snd/musicaFundo.mp3';
  musicaFundo.load();
  musicaFundo.volume = 0.2;
  musicaFundo.loop = true;
  
  fundo = new Image();
  fundo.src = 'img/fundo.png';
};
